package cat.itb.cityquiz.Screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import cat.itb.cityquiz.R;


public class ResultsFragments extends Fragment {

    private QuizViewModel quizViewModel;
    Button restartButton;

    public static ResultsFragments newInstance() {
        return new ResultsFragments();
    }

    @Nullable
    @Override
    //Primero en ejecturarse
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.third_fragment, container, false);
    }

    @Override
    //Tercero en ejecturarse

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);

        TextView Score;
        int r = quizViewModel.getGame().getNumCorrectAnswers();

        Score = getView().findViewById(R.id.Puntuacion);
        Score.setText(Integer.toString(r));
    }
    //Segundo en ejecturarse

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        restartButton = view.findViewById(R.id.buttonRestart);
        restartButton.setOnClickListener(this::restart);
    }

    private void restart(View view) {
        Navigation.findNavController(view).navigate(R.id.action_resultsFragments_to_firstFragment);
    }


}
