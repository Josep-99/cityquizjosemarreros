package cat.itb.cityquiz.Screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import cat.itb.cityquiz.R;


public class StartFragment extends Fragment {

    Button button;
    private QuizViewModel quizViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.first_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button=view.findViewById(R.id.buttonStart);
        button.setOnClickListener(this::startGame);
    }
    private void startGame(View view) {
        //Toast.makeText(getActivity().getApplicationContext(), "Funciona", Toast.LENGTH_LONG).show();

        /*
        Game newGame = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);

        gameLogic.answerQuestions(newGame,6);
         */
        quizViewModel.StarQuiz();
        Navigation.findNavController(view).navigate(R.id.action_firstFragment_to_secondFragment);
        /*
        quizViewModel = ViewModelProviders.of(getActivity()).get(SecondViewModel.class);
        */
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        // TODO: Use the ViewModel
    }
}
