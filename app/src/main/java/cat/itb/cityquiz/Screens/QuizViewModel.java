package cat.itb.cityquiz.Screens;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Answer;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    Game game;
    //MutableLiveData<int>
    int scoreFinal = 0;
    Answer answer;

    public void StarQuiz() {
        game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
    }


/*
    public int FinalScore(){
        return game.getScore();
    }
    */

    public void NextQuestion(int position){
        gameLogic.answerQuestions(game, position);
    }

    public Game getGame() {
        return game;
    }

}