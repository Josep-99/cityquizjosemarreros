package cat.itb.cityquiz.Screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import java.util.Objects;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Game;


public class QuizFragment extends Fragment implements View.OnClickListener {

    ImageView imageView;
    private QuizViewModel quizViewModel;
    Button boton1;
    Button boton2;
    Button boton3;
    Button boton4;
    Button boton5;
    Button boton6;

    String nombre1;
    String nombre2;
    String nombre3;
    String nombre4;
    String nombre5;
    String nombre6;
    String correct;


    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.second_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageView = getView().findViewById(R.id.imageView);

        boton1 = view.findViewById(R.id.respuesta1);
        boton2 = view.findViewById(R.id.respuesta2);
        boton3 = view.findViewById(R.id.respuesta3);
        boton4 = view.findViewById(R.id.respuesta4);
        boton5 = view.findViewById(R.id.respuesta5);
        boton6 = view.findViewById(R.id.respuesta6);

        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        quizViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        Game game = quizViewModel.getGame();
            display(game);
    }


    private void display(Game game) {

        nombre1 = game.getCurrentQuestion().getPossibleCities().get(0).getName();
        nombre2 = game.getCurrentQuestion().getPossibleCities().get(1).getName();
        nombre3 = game.getCurrentQuestion().getPossibleCities().get(2).getName();
        nombre4 = game.getCurrentQuestion().getPossibleCities().get(3).getName();
        nombre5 = game.getCurrentQuestion().getPossibleCities().get(4).getName();
        nombre6 = game.getCurrentQuestion().getPossibleCities().get(5).getName();

        boton1.setText(nombre1);
        boton2.setText(nombre2);
        boton3.setText(nombre3);
        boton4.setText(nombre4);
        boton5.setText(nombre5);
        boton6.setText(nombre6);

        correct = game.getCurrentQuestion().getCorrectCity().getName();

        String fileName = ImagesDownloader.scapeName(correct);
        int resId = Objects.requireNonNull(getActivity()).getResources().getIdentifier(fileName, "drawable", getActivity().getPackageName());
        imageView.setImageResource(resId);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.respuesta1:
                quizViewModel.NextQuestion(0);

                break;

            case R.id.respuesta2:
                quizViewModel.NextQuestion(1);
                break;
            case R.id.respuesta3:
                quizViewModel.NextQuestion(2);
                break;

            case R.id.respuesta4:
                quizViewModel.NextQuestion(3);
                break;
            case R.id.respuesta5:
                quizViewModel.NextQuestion(4);
                break;

            case R.id.respuesta6:
                quizViewModel.NextQuestion(5);
                break;
        }
        Game game = quizViewModel.getGame();
        if(game.isFinished()){
            Navigation.findNavController(view).navigate(R.id.action_secondFragment_to_resultsFragments);
        } else
            display(game);
    }
}
