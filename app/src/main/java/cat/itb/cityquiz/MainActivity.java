package cat.itb.cityquiz;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;
import cat.itb.cityquiz.R;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        NavigationUI.setupActionBarWithNavController(this, findNavController(this, R.id.nav_host_fragment));

    }



    /*
    @Override
    public boolean onSupportNavigateUp() {
        // Afegir si es vol mostrar el 'back' a la actionBar
        return findNavController(this, R.id.nav_host_fragment).navigateUp();
    }
     */
}
