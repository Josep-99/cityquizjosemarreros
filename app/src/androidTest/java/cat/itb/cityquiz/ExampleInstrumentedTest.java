package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void Test1(){
        //test automatico de como funciona la aplicacion
        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta1)).perform(click());
        onView(withId(R.id.respuesta2)).perform(click());
        onView(withId(R.id.respuesta3)).perform(click());
        onView(withId(R.id.respuesta4)).perform(click());
        onView(withId(R.id.respuesta5)).perform(click());
        onView(withId(R.id.buttonRestart)).perform(click());
    }

    @Test
    public void Test2() {
        //Comprobacion de que cada boton en cada fragment hace la navegacion correctamente

        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta1)).perform(click());
        onView(withId(R.id.respuesta1)).perform(click());
        onView(withId(R.id.respuesta1)).perform(click());
        onView(withId(R.id.respuesta1)).perform(click());
        onView(withId(R.id.respuesta1)).perform(click());
        onView(withId(R.id.buttonRestart)).perform(click());
        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta2)).perform(click());
        onView(withId(R.id.respuesta2)).perform(click());
        onView(withId(R.id.respuesta2)).perform(click());
        onView(withId(R.id.respuesta2)).perform(click());
        onView(withId(R.id.respuesta2)).perform(click());
        onView(withId(R.id.buttonRestart)).perform(click());
        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta3)).perform(click());
        onView(withId(R.id.respuesta3)).perform(click());
        onView(withId(R.id.respuesta3)).perform(click());
        onView(withId(R.id.respuesta3)).perform(click());
        onView(withId(R.id.respuesta3)).perform(click());
        onView(withId(R.id.buttonRestart)).perform(click());
        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta4)).perform(click());
        onView(withId(R.id.respuesta4)).perform(click());
        onView(withId(R.id.respuesta4)).perform(click());
        onView(withId(R.id.respuesta4)).perform(click());
        onView(withId(R.id.respuesta4)).perform(click());
        onView(withId(R.id.buttonRestart)).perform(click());
        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta5)).perform(click());
        onView(withId(R.id.respuesta5)).perform(click());
        onView(withId(R.id.respuesta5)).perform(click());
        onView(withId(R.id.respuesta5)).perform(click());
        onView(withId(R.id.respuesta5)).perform(click());
        onView(withId(R.id.buttonRestart)).perform(click());
        onView(withId(R.id.buttonStart)).perform(click());
        onView(withId(R.id.respuesta6)).perform(click());
        onView(withId(R.id.respuesta6)).perform(click());
        onView(withId(R.id.respuesta6)).perform(click());
        onView(withId(R.id.respuesta6)).perform(click());
        onView(withId(R.id.respuesta6)).perform(click());
    }
}
